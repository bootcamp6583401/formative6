import java.io.FileWriter;
import java.io.IOException;

public class Task1 {

    public static void main(String[] args) {
        Thread bogorThread = new Thread(new LicensePlateRunner('F', "Bogor"));
        Thread jakartaThread = new Thread(new LicensePlateRunner('B', "Jakarta"));
        Thread tangerangThread = new Thread(new LicensePlateRunner('A', "Tangerang"));

        bogorThread.start();
        jakartaThread.start();
        tangerangThread.start();
    }

    static class LicensePlateRunner implements Runnable {
        private final char startingLetter;
        private final String cityName;

        public LicensePlateRunner(char startingLetter, String cityName) {
            this.startingLetter = startingLetter;
            this.cityName = cityName;
        }

        @Override
        public void run() {
            String fileName = cityName + ".txt";

            try (FileWriter fileWriter = new FileWriter(fileName)) {
                generate(fileWriter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void generate(FileWriter fileWriter) throws IOException {
            for (int i = 1; i < 10000; i++) {
                String numberPart = String.format("%04d", i);
                for (char firstChar = 'A'; firstChar <= 'Z'; firstChar++) {
                    for (char secondChar = 'A'; secondChar <= 'Z'; secondChar++) {
                        String licensePlate = startingLetter + " " + numberPart.replace("0", "") + " " + firstChar
                                + secondChar + "\n";
                        fileWriter.write(licensePlate);
                    }
                }
            }
        }
    }
}