import java.util.Scanner;
import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        String[] names = {
                "Michael Scott",
                "Jim Halpert",
                "Pam Beesly",
                "Dwight Schrute",
                "Angela Martin",
                "Kevin Malone",
                "Stanley Hudson",
                "Oscar Martinez",
                "Andy Bernard",
                "Ryan Howard"
        };

        String randomizedName = scn.nextLine();

        int closestIndex = searchClosestMatch(randomizedName, names);
        System.out.println(names[closestIndex]);

        scn.close();
    }

    public static int searchClosestMatch(String randomizedName, String[] nameList) {

        int currentTotalCorrectChars = 0;
        int closestIndex = 0;

        for (int i = 0; i < nameList.length; i++) {
            int totalCorrectChars = 0;

            String[] randomizedSplit = randomizedName.split(" ");
            String[] splitName = nameList[i].split(" ");

            // loop words
            for (int j = 0; j < (randomizedSplit.length > splitName.length ? splitName.length
                    : randomizedSplit.length); j++) {
                char[] randomWordCharArr = randomizedSplit[j].toCharArray();
                char[] nameWordCharArr = splitName[j].toCharArray();
                Arrays.sort(randomWordCharArr);
                Arrays.sort(nameWordCharArr);

                // count correct letters
                for (int k = 0; k < (randomWordCharArr.length > nameWordCharArr.length ? nameWordCharArr.length
                        : randomWordCharArr.length); k++) {
                    if (randomWordCharArr[k] == nameWordCharArr[k]) {
                        totalCorrectChars++;
                    }
                }
            }

            if (totalCorrectChars > currentTotalCorrectChars) {
                closestIndex = i;
                currentTotalCorrectChars = totalCorrectChars;
            }
        }

        return closestIndex;
    }
}
