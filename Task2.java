import java.util.Random;

public class Task2 {
    public static void main(String[] args) {
        Random random = new Random();
        String[] names = {
                "Michael Scott",
                "Jim Halpert",
                "Pam Beesly",
                "Dwight Schrute",
                "Angela Martin",
                "Kevin Malone",
                "Stanley Hudson",
                "Oscar Martinez",
                "Andy Bernard",
                "Ryan Howard"
        };

        String name = names[random.nextInt(names.length)];
        System.out.println("Nama: " + name);

        String scrambledString = randomizeName(name);
        System.out.println("Diacak: " + scrambledString);
    }

    private static String randomizeName(String str) {
        String[] words = str.split(" ");
        String[] randomized = new String[words.length];

        for (int j = 0; j < words.length; j++) {
            char[] charArray = words[j].toCharArray();
            Random random = new Random();

            for (int i = 0; i < charArray.length; i++) {
                int randomIndex = random.nextInt(charArray.length);

                char temp = charArray[i];
                charArray[i] = charArray[randomIndex];
                charArray[randomIndex] = temp;
            }

            randomized[j] = new String(charArray);
        }

        return String.join(" ", randomized);

    }
}
